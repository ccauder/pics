import axios from 'axios';

export default axios.create({
  baseURL: 'https://api.unsplash.com',
  headers: {
    Authorization:
      'Client-ID Or1br2FtXQllItC5_Rqfx0yBTEcG90adRbrDELzIzBM'
  }
});
